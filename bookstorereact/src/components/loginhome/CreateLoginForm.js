import React from "react";
import TextInput from "../common/TextInput";

export default function CreateLoginForm(props) {
  return (
    <div className="jumbotron">
      <form onSubmit={props.onSubmit}>
        <TextInput
          id="username"
          label="username"
          onChange={props.onChange}
          onSubmit={props.onSubmit}
          name="username"
          value={props.login.username}
        />
        <TextInput
          type="password"
          id="password"
          label="password"
          onChange={props.onChange}
          onSubmit={props.onSubmit}
          name="password"
          value={props.login.password}
        />

        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}
