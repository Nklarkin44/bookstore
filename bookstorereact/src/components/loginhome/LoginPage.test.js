import React from "react";
import Enzyme, { shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import LoginPage from "./LoginPage";

Enzyme.configure({ adapter: new Adapter() });

describe("LoginPage", () => {
  const wrapper = shallow(<LoginPage />);
  it("should contain parent div", () => {
    const parentDiv = wrapper.find("div");
    expect(parentDiv.length).toBe(1);
  });
  it("Should have welcoming in h1", () => {
    const h1 = wrapper.find("h1");
    expect(h1.text()).toBe(
      "Welcome To Revs Bookstore, please login using the form below:"
    );
  });
});
