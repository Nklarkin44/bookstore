import React, { useState } from "react";
import CreateLoginForm from "./CreateLoginForm";
import { toast } from "react-toastify";

export default function LoginPage(props) {
  const [login, setLogin] = useState({
    username: "",
    password: "",
  });

  function handleChange(event) {
    setLogin({ ...login, [event.target.name]: event.target.value });
  }

  function handleSubmit(event) {
    event.preventDefault();
    let hardcodedProfile = {
      username: "admin",
      password: "adminpassword",
    };
    if (
      login.username === hardcodedProfile.username &&
      login.password === hardcodedProfile.password
    ) {
      //I could have used sessions to store a token which I would check before allowing access to other types
      //Instead I will redirect them to the book search
      toast.success("You have been successfully logged in!");
      props.history.push("/books");
    } else {
      toast.error("The credentials entered are incorrect, please try again.");
    }
  }

  return (
    <div>
      <h1>Welcome To Revs Bookstore, please login using the form below:</h1>
      <CreateLoginForm
        onSubmit={handleSubmit}
        onChange={handleChange}
        login={login}
      />
    </div>
  );
}
