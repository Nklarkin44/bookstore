import React from "react";
import { Link } from "react-router-dom";

export default function NotFoundPage() {
  return (
    <>
      <h1>404. Page not found.</h1>
      <h3>
        Please use the button below to take you back to the login home page. You
        will need to relogin.
      </h3>
      <Link to="/">
        <button className="btn btn-primary">Back to login</button>
      </Link>
    </>
  );
}
