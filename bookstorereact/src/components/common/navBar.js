import React from "react";
import { NavLink } from "react-router-dom";
//the navigation bar used for routing.
const NavBar = () => {
  const activeStyle = { color: "pink" };
  return (
    <nav>
      <NavLink to="/" activeStyle={activeStyle} exact>
        HomeLogin
      </NavLink>
      {" | "}
      <NavLink to="/books" activeStyle={activeStyle}>
        Books
      </NavLink>
      {" | "}
      <NavLink to="/search" activeStyle={activeStyle}>
        Book Search
      </NavLink>
      {" | "}
      <NavLink to="/about" activeStyle={activeStyle}>
        About
      </NavLink>
    </nav>
  );
};

export default NavBar;
