import React, { useState } from "react";
import CreateBookForm from "./CreateBookForm";
import { toast } from "react-toastify";
//import { Redirect } from "react-router-dom";

export default function CreateBook(props) {
  const [book, setBook] = useState({
    book_id: "",
    title: "",
    authors: "",
  });

  function handleChange(event) {
    setBook({ ...book, [event.target.name]: event.target.value });
  }

  function handleSubmit(event) {
    event.preventDefault();

    var url = new URL("http://18.189.43.239:5000/book/add/"),
      params = { title: book.title, authors: +book.authors };
    Object.keys(params).forEach((key) =>
      url.searchParams.append(key, params[key])
    );
    fetch(url, { method: "POST" })
      .then((data) => {
        console.log(data);
      })
      .then((result) => {
        console.log(book);
        toast.success("New Book Created!");
        props.history.push("/books");
      })
      .catch((err) => {
        toast.error("There was an error while creating the book");
      });
  }

  //   event.preventDefault();
  //   fetch("http://18.189.43.239:5000/book/add", {
  //     method: "POST",
  //     body: JSON.stringify(book),
  //   })
  //     .then((response) => response.json())
  //     .then((data) => {
  //       console.log(data);
  //       setBook(data);
  //     })
  //     .then((result) => {
  //       console.log(book);
  //       toast.success("New Book Created!");
  //       props.history.push("/books");
  //     })
  //     .catch((err) => {
  //       toast.error("There was an error while creating the book");
  //     });
  // }

  return (
    <div>
      <h3>Fill out form to insert your book into our database!</h3>
      <CreateBookForm
        onSubmit={handleSubmit}
        onChange={handleChange}
        book={book}
      />
    </div>
  );
}
