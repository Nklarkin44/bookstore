import React, { useState } from "react";
import SearchBooksForm from "./SearchBooksForm";
import { toast } from "react-toastify";
import SearchedBook from "./SearchedBook";

export default function SearchBooks(props) {
  //state to keep track of the book that will be displayed in Searched Book.
  const [book, setBook] = useState({
    book_id: "",
    title: "",
    authors: "",
  });
  //state to monitor searchform
  const [bookid, setBookID] = useState({
    book_id: "",
  });

  function handleChange(event) {
    setBookID({ ...bookid, [event.target.name]: event.target.value });
  }

  function handleSubmit(event) {
    event.preventDefault();
    fetch("http://18.189.43.239:5000/book/" + bookid.book_id, {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setBook(data[0]);
      })
      .then((result) => {
        toast.success("Specified book was found!");
      })
      .catch((err) => {
        toast.error("There was an error while searching for the book");
      });
  }

  return (
    <div>
      <h2>Put in an ID to find your book!</h2>
      <SearchBooksForm
        onSubmit={handleSubmit}
        onChange={handleChange}
        bookid={bookid}
      />
      <h4>The searched book will populate below:</h4>
      <SearchedBook book={book} />
    </div>
  );
}
