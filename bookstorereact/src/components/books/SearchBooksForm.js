import React from "react";
import TextInput from "../common/TextInput";

export default function SearchBooksForm(props) {
  return (
    <form onSubmit={props.onSubmit}>
      <TextInput
        id="book_id"
        label="Book ID"
        onChange={props.onChange}
        onSubmit={props.onSubmit}
        name="book_id"
        value={props.bookid.book_id || ""}
        type="number"
      />

      <button type="submit" className="btn btn-primary">
        Search
      </button>
    </form>
  );
}
