import React, { useState } from "react";
import DeleteBookForm from "./DeleteBookForm";
import { toast } from "react-toastify";

export default function DeleteBook(props) {
  const [book, setBook] = useState({
    book_id: "", //you might need full book params
  });

  function handleChange(event) {
    setBook({ ...book, [event.target.name]: event.target.value });
  }

  function handleSubmit(event) {
    event.preventDefault();

    fetch("http://18.189.43.239:5000/book/delete/" + book.book_id, {
      method: "POST",
      //  headers: { "Content-Type": "application/json" },
      // body: JSON.stringify(book),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setBook(data);
      })
      .then((result) => {
        toast.success("Specified book was deleted!");
        props.history.push("/books");
      })
      .catch((err) => {
        toast.error("There was an error while deleting the book");
      });
  }

  return (
    <div>
      <h3>
        Please enter the ID and we will remove that book from the database.
      </h3>
      <DeleteBookForm
        onSubmit={handleSubmit}
        onChange={handleChange}
        book={book}
      />
    </div>
  );
}
