import React from "react";
import { Link } from "react-router-dom";
import ListBooks from "./ListBooks";

export default class BookPage extends React.Component {
  state = {
    books: [],
  };

  componentDidMount() {
    fetch("http://18.189.43.239:5000/books")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        this.setState({
          books: data,
        });
      })
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <div>
        <Link to="/books/create">
          <button className="btn btn-primary">Insert Book</button>
        </Link>
        {" - "}
        <Link to="/books/delete">
          <button className="btn btn-primary">Remove Book</button>
        </Link>
        {" - "}
        <Link to="/search">
          <button className="btn btn-primary">Search by ID</button>
        </Link>
        <h3>Revs Book Supply</h3>
        <ListBooks books={this.state.books} />
      </div>
    );
  }
}
