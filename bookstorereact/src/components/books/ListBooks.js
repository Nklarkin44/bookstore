import React from "react";

export default function ListBooks(props) {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Author</th>
        </tr>
      </thead>
      <tbody>
        {props.books.map((book) => {
          return (
            <tr key={book.book_id}>
              <td>{book.book_id}</td>
              <td>{book.title}</td>
              <td>{book.authors}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
