import React from "react";

export default function SearchedBook(props) {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Author</th>
        </tr>
      </thead>
      <tbody>
        <tr key={props.book.book_id}>
          <td>{props.book.book_id}</td>
          <td>{props.book.title}</td>
          <td>{props.book.authors}</td>
        </tr>
      </tbody>
    </table>
  );
}
