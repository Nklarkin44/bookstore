import React from "react";
import TextInput from "../common/TextInput";

export default function DeleteBookForm(props) {
  return (
    <form onSubmit={props.onSubmit}>
      <TextInput
        id="book_id"
        label="Book ID"
        onChange={props.onChange}
        onSubmit={props.onSubmit}
        name="book_id"
        value={props.book.book_id || ""}
        type="number"
      />

      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
}
