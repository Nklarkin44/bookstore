import React from "react";
import TextInput from "../common/TextInput";

export default function CreateBookForm(props) {
  return (
    <form onSubmit={props.onSubmit}>
      <TextInput
        id="title"
        label="Title"
        onChange={props.onChange}
        onSubmit={props.onSubmit}
        name="title"
        value={props.book.title || ""}
      />
      <TextInput
        id="authors"
        label="Authors"
        onChange={props.onChange}
        onSubmit={props.onSubmit}
        name="authors"
        value={props.book.authors || ""}
      />

      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
}
