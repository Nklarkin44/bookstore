import React from "react";
import LoginPage from "./components/loginhome/LoginPage";
import "./App.css";
import AboutPage from "./components/about/AboutPage";
import BookPage from "./components/books/BookPage";
import SearchBooks from "./components/books/SearchBooks";
import CreateBook from "./components/books/CreateBook";
import DeleteBook from "./components/books/DeleteBook";
import { Switch, Route } from "react-router-dom";
import NavBar from "./components/common/NavBar";
import NotFoundPage from "./components/notfound/NotFoundPage";
import { ToastContainer } from "react-toastify";

function App() {
  return (
    <div className="container-fluid">
      <NavBar />
      <ToastContainer autoClose={3000} hideProgressBar />
      <Switch>
        <Route exact path="/" component={LoginPage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/books/delete" component={DeleteBook} />
        <Route path="/books/create" component={CreateBook} />
        <Route path="/search" component={SearchBooks} />
        <Route path="/books" component={BookPage} />
        <Route path="/login" component={LoginPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
}

export default App;
